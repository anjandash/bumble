package com.pixellato.bumble;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.pixellato.bumble.R.id.radioButton;


public class ShareActivity extends AppCompatActivity {

    Bitmap mainPhoto;
    Uri mainUri;
    boolean shared, notShown = true;
    int clickCount = 0;
    Intent globalShareIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        clickCount = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        builder.setTitle("Success!");
        builder.setMessage("Your message is hidden!"+"\n");
        builder.setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setCancelable(true);

        final AlertDialog closeDialog= builder.create();
        closeDialog.show();

//        final Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            public void run() {
//                closeDialog.dismiss();
//                timer.cancel();
//            }
//        }, 3000);

        Bitmap bitmap = null;
        Intent intent = getIntent();
        Uri myUri = Uri.parse(intent.getStringExtra("imageUri"));

        try{
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), myUri);
        } catch (Exception e){
            System.out.println(0);
        }

        ImageView imageView = (ImageView)findViewById(R.id.imageView4);
        imageView.setImageBitmap(bitmap);
        mainPhoto = bitmap;
        mainUri = myUri;
        saveImage(mainPhoto, 1);

        if(hasSecret(mainPhoto)){
            int strLength = Integer.parseInt(get_length(mainPhoto, 0));
            String hiddenMessage = decode_text(mainPhoto, strLength, 0);

            ((SharedData) ShareActivity.this.getApplication()).setSomeVariable(hiddenMessage);
        }

        //default
        globalShareIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","", null));
        globalShareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject:");
        globalShareIntent.putExtra(Intent.EXTRA_TEXT, "");
        globalShareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mainUri.toString()));

        delImage();

        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);
                
                switch (index) {
                    case 0:
                        globalShareIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","", null));
                        globalShareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject:");
                        globalShareIntent.putExtra(Intent.EXTRA_TEXT, "");
                        globalShareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mainUri.toString()));
                        break;

                    case 1:
                        globalShareIntent = new Intent(Intent.ACTION_SEND);
                        globalShareIntent.setType("image/*");
                        globalShareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mainUri.toString()));
                        if(notShown) {
                            (new AlertDialog.Builder(ShareActivity.this))
                                .setTitle("Warning!")
                                .setMessage("Sharing the image with other apps " +
                                        "might cause this image to be compressed, " +
                                        "in which case you will lose the secret text! \n")
                                .setPositiveButton("SHARE ANYWAY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Button button = (Button) findViewById(R.id.button);
                                        button.performClick();
                                    }
                                })
                                .setNegativeButton("BACK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                            notShown = false;
                        }
                        break;
                }
            }
        });


    }

    public void handleRadio(View view){
    }



    public void delImage() {

        Context context = ShareActivity.this;

        context.getContentResolver().delete(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DESCRIPTION + "='BMBENCRIMAGE'", null
        );

        context.getContentResolver().delete(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DESCRIPTION + "='BMBCROPIMAGE'", null
        );

        context.getContentResolver().delete(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DESCRIPTION + "='BMBBASEIMAGE'", null
        );
        callBroadCast();
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    public void textShare(View view){
        startActivity(Intent.createChooser(globalShareIntent, "Share Image"));
        shared = true;
    }

    private String decode_text(Bitmap imagefile, int lenns, int offset){
        int x = 0, y = 0;
        Bitmap image = imagefile.copy(Bitmap.Config.ARGB_8888, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] imageByteArray = stream.toByteArray();

        if(lenns + offset > imageByteArray.length)
        {
            throw new IllegalArgumentException("File not long enough!");
        }

        String manix = "";

        for(int i=0; i<lenns+3; ++i)
        {

            String street = "";
            for(int bit=7; bit>=0; --bit, ++offset)
            {

                int rgbV = image.getPixel(x, y);

                int a = (rgbV >> 24) & 0xFF;
                int r = (rgbV >> 16) & 0xFF;
                int g = (rgbV >> 8) & 0xFF;
                int b = (rgbV >> 0) & 0xFF;

                int lsb = ((b % 2));
                street += Integer.toString(lsb);


                if((y+1)< image.getHeight()){
                    y++;
                } else if ((x+1)< image.getWidth()){
                    x++;
                    y=0;
                } else {
                    System.out.println("EOLOOLEO1111");
                }

            }
            System.out.println("---STREET TEST");
            System.out.println(street);
            int charCode = Integer.parseInt(street, 2);
            String str = new Character((char)charCode).toString();
            System.out.println((str));
            System.out.println("---STREET TEST");

            manix += str;
        }

        System.out.println("EOLOOLEO2222");
        manix = manix.substring(3);
        return manix;
    }

    private String get_length(Bitmap imagefile2, int offset){
        int x = 0, y = 0;
        Bitmap image = imagefile2.copy(Bitmap.Config.ARGB_8888, true);

        String manix = "";

        for(int i=0; i<3; ++i)
        {
            String street = "";
            for(int bit=7; bit>=0; --bit, ++offset)
            {

                int rgbV = image.getPixel(x, y);

                int a = (rgbV >> 24) & 0xFF;
                int r = (rgbV >> 16) & 0xFF;
                int g = (rgbV >> 8) & 0xFF;
                int b = (rgbV >> 0) & 0xFF;

                int lsb = ((b % 2));



                street += Integer.toString(lsb);


                if((y+1)< image.getHeight()){
                    y++;
                } else if ((x+1)< image.getWidth()){
                    x++;
                    y=0;
                } else {
                    System.out.println("Reached end of image file!");
                }

            }
            System.out.println("---STREET TEST");
            System.out.println(street);
            int charCode = Integer.parseInt(street, 2);
            String str = new Character((char)charCode).toString();
            System.out.println((str));
            System.out.println("---STREET TEST");

            manix += str;
        }

        return manix;
    }

    private boolean hasSecret(Bitmap imageFile){

        String lengthChk = get_length(imageFile, 0);
        int len;
        boolean pars;

        pars = isParsable(lengthChk);
        if(pars){
            len = Integer.parseInt(lengthChk);
            if(len > 0 && len < 1000){
                return true;
            }else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isParsable(String input){
        boolean parsable = true;
        try{
            Integer.parseInt(input);
        }catch(NumberFormatException e){
            parsable = false;
        }
        return parsable;
    }

    public Uri saveImage(Bitmap nowPhoto, int x) {

        Bitmap icon = nowPhoto;
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        if (x == 0) {

            boolean bool = shouldAskPermission();
            if (bool) {
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                int permsRequestCode = 200;

                requestPermissions(perms, permsRequestCode);
            }
        } else {

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/BumbleConv");
            myDir.mkdirs();

            String fname = timeStamp + "-shr-saved.jpg";
            File f = new File (myDir, fname);
            if (f.exists ()) f.delete ();

            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                icon.compress(Bitmap.CompressFormat.PNG, 100, fo);
                fo.flush();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return Uri.parse("file:///sdcard/BumbleConv/"+timeStamp+"-shr-saved.jpg");
    }

    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        if(permsRequestCode == 200){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // save file
                System.out.println("NOTE: Saving Image Locally!");
                saveImage(mainPhoto, 1);

            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(shared == false) {
            (new AlertDialog.Builder(this))
                    .setTitle("Share image")
                    .setMessage("Would you like to share the image\nbefore you go back?")
                    .setNegativeButton("BACK TO HOMEPAGE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //finish();
                            Intent intent = new Intent(ShareActivity.this, MainActivity.class);
                            ShareActivity.this.startActivity(intent);
                        }
                    })
                    .setPositiveButton("STAY HERE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
        } else {
            (new AlertDialog.Builder(this))
                    .setTitle("Return")
                    .setMessage("Back to homepage?")
                    .setPositiveButton("BACK TO HOMEPAGE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //finish();
                            Intent intent = new Intent(ShareActivity.this, MainActivity.class);
                            ShareActivity.this.startActivity(intent);
                        }
                    })
                    .setNegativeButton("EXIT APP", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        String hiddenMsg = ((SharedData) ShareActivity.this.getApplication()).getSomeVariable();
        if(hiddenMsg != null){
            MenuItem alertButton = menu.findItem(R.id.action_alert);
            alertButton.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_alert) {

            String hiddenMsg = ((SharedData) ShareActivity.this.getApplication()).getSomeVariable();
            clickCount++;
            if(true){
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
                builder.setTitle("The hidden message is:");
                builder.setMessage(hiddenMsg+"\n");
                builder.setCancelable(true);
                builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                final AlertDialog closeDialog= builder.create();
                closeDialog.show();

//                final Timer timer = new Timer();
//                timer.schedule(new TimerTask() {
//                    public void run() {
//                        closeDialog.dismiss();
//                        timer.cancel();
//                    }
//                }, 2000);
                clickCount = 0;
            }
            return true;
        }
        if (id == R.id.action_save) {

            Toast toast=Toast.makeText(getApplicationContext(),"Saving...",Toast.LENGTH_SHORT);
            toast.show();

            saveImage(mainPhoto, 1);
            toast.cancel();

            Toast toastSaved=Toast.makeText(getApplicationContext(),"Image saved in folder /BumbleConv.",Toast.LENGTH_LONG);
            toastSaved.show();
            return true;
        }

        if (id == R.id.action_cancel) {

            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
